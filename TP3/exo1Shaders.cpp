
/******************************************************************************
 ******************************* INCLUDE SECTION ******************************
 ******************************************************************************/

// STL
#include <iostream>
#include <vector>

// System
#include <cstdio>

// Graphics
// - GLEW (always before "gl.h")
#include <GL/glew.h>
// - GL
#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/gl.h>
// - GLUT
#include <GL/glut.h>

// glm
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

/******************************************************************************
 ****************************** NAMESPACE SECTION *****************************
 ******************************************************************************/

/******************************************************************************
 ************************* DEFINE AND CONSTANT SECTION ************************
 ******************************************************************************/

// VBO (vertex buffer object) : used to store positions coordinates at each point
GLuint positionBuffer;
// VBO (vertex buffer object) : used to store normales at each point
GLuint normalBuffer;
// VBO (vertex buffer object) : used to store positions index
GLuint indexBuffer;
// VAO (vertex array object) : used to encapsulate several VBO
GLuint vertexArray;

GLuint quadArray;
GLuint quadVertexArray;

// Mesh
int numberOfVertices_;
int numberOfIndices_;

// Shader program
GLuint shaderProgram, lightShaderProgram;
GLuint projectionMatrixLocation, viewMatrixLocation, modelMatrixLocation, lightPosLocation, lightColorLocation, objectColorLocation, lightModelMatrixLocation;

// View Matrix
float eyez = 0.0f;
float eyex = 0.0f;
glm::vec3 eye = glm::vec3(-1.f, 0.f, 1.f);
glm::vec3 center = glm::vec3(0.f, 0.f, 0.f);
glm::vec3 up = glm::vec3(0.f, 1.f, 0.f);
// View matrix
glm::mat4 viewMatrix = glm::lookAt(eye, center, up);

// Projection matrix
float fovY = 45.f;
float aspect = 1.f;
float zNear = 0.f;
float zFar = 200.f;
glm::mat4 projectionMatrix = glm::perspective(fovY, aspect, zNear, zFar);

// Model matrix
glm::mat4 transform;
glm::mat4 modelMatrix = glm::translate(transform,  glm::vec3(0.f, 0.f, 0.f));

glm::vec3 lightPos(0.f, 1.f, 0.f);
glm::vec3 lightColor(0.25f, 0.72f, 0.1f);
glm::vec3 objectColor(1.0f, .0f, 0.1f);

/******************************************************************************
 ***************************** TYPE DEFINITION ********************************
 ******************************************************************************/

/******************************************************************************
***************************** METHOD DEFINITION ******************************
******************************************************************************/

void updateViewMatrix()
{
    eye = glm::vec3(eyex, 0.f, eyez);
    viewMatrix = glm::lookAt(eye, center, up);
}


/******************************************************************************
 ***************************** METHOD DEFINITION ******************************
 ******************************************************************************/

bool initialize();
bool checkExtensions();
bool initializeArrayBuffer();
bool initializeVertexArray();
bool initializeShaderProgram();
bool finalize();

/******************************************************************************
 * Procdural mesh
 ******************************************************************************/
void waves( std::vector<glm::vec3>& points, std::vector<glm::vec3>& normals, std::vector<GLuint>& triangleIndices, int nb )
{
    points.resize(nb*nb);
    normals.resize(nb*nb);
    for(int j=0;j<nb;++j)
    {
        for(int i=0;i<nb;++i)
        {
            int k=j*nb+i;
            float x = 6.0f/nb*j-3.000001f;
            float y = 6.0f/nb*i-3.000001f;

            // Rescale
            x /= 4.f;
            y /= 4.f;

            float r = std::sqrt(x*x+y*y);
            printf("%f ", r);
            //            float h = 0.4f*std::sin(M_PI/2.0+r*7);
            float h = 0.4f*(1-r/5)*std::sin(M_PI/2.0+r*5);
            //            points[k] = {x,y,h};
            points[k] = {x,h,y};

            //            float dh = 7*0.4f*std::cos(M_PI/2.0+r*7);

            float dh = -0.4/5*std::sin(M_PI/2.0+r*5) +
                0.4f*(1-r/5)*5*std::cos(M_PI/2.0+r*5);

            glm::vec3 n = {-x/r*dh,-y/r*dh,1};
            normals[k]= glm::normalize(n);
        }
    }
    /* for ( auto& p : points )
       {
       p.x /= 100.f;
       p.y /= 100.f; 
       p.z /= 100.f;  
       }*/

    triangleIndices.reserve(6*(nb-1)*(nb-1));
    for(int j=1;j<nb;++j)
        for(int i=1;i<nb;++i)
        {
            int k=j*nb+i;
            triangleIndices.push_back(k);
            triangleIndices.push_back(k-nb);
            triangleIndices.push_back(k-nb-1);

            triangleIndices.push_back(k);
            triangleIndices.push_back(k-nb-1);
            triangleIndices.push_back(k-1);
        }
}

/******************************************************************************
 * Initialize all
 ******************************************************************************/
bool initialize()
{
	std::cout << "Initialize all..." << std::endl;

	bool statusOK = true;

	if ( statusOK )
	{
		checkExtensions();
	}

	if ( statusOK )
	{
		initializeArrayBuffer();
	}

	if ( statusOK )
	{
		initializeVertexArray();
	}

	if ( statusOK )
	{
		initializeShaderProgram();
	}

	return statusOK;
}

/******************************************************************************
 * Finalize all
 ******************************************************************************/
bool finalize()
{
	bool statusOK = true;

	std::cout << "Finalize all..." << std::endl;

	return statusOK;
}

/******************************************************************************
 * Check GL extensions
 ******************************************************************************/
bool checkExtensions()
{
	bool statusOK = true;

	std::cout << "Check GL extensions..." << std::endl;

	return statusOK;
}

/******************************************************************************
 * Initialize array buffer
 ******************************************************************************/
bool initializeArrayBuffer()
{
    bool statusOK = true;

    std::cout << "Initialize array buffer..." << std::endl;

    // In this example, we want to display one triangle

    // Buffer of positions on CPU (host)
    std::vector<glm::vec3> points;
    std::vector<glm::vec3> normals;
    std::vector<GLuint> triangleIndices;
    int nb = 100;
    waves( points, normals, triangleIndices, nb );
    numberOfVertices_ = points.size();
    numberOfIndices_ = triangleIndices.size();

    // Positions
    glGenBuffers(1, &positionBuffer);
    // buffer courant a manipuler
    glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
    // definit la taille du buffer et le remplit
    glBufferData(GL_ARRAY_BUFFER, numberOfVertices_ * sizeof(glm::vec3), points.data(), GL_STATIC_DRAW);
    // buffer courant : rien
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Index
    glGenBuffers(1, &indexBuffer);
    // buffer courant a manipuler
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    // definit la taille du buffer et le remplit
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, numberOfIndices_ * sizeof(GLuint), triangleIndices.data(), GL_STATIC_DRAW);
    // buffer courant : rien
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


    // Quads
    const std::vector< float > vertices = {
        // FRONT
        -0.5f, -0.5f, .5f, 
        0.5f, -0.5f, .5f, 
        0.5f,  0.5f, .5f, 
        -0.5f, 0.5f, .5f 
    };

    // Create a GPU buffer (device)
    // - generate a VBO ID
    glGenBuffers( 1, &quadArray );
    // - bind VBO as current buffer (in OpenGL state machine)
    glBindBuffer( GL_ARRAY_BUFFER, quadArray );
    // - send data from CPU (host) to GPU (device)
    glBufferData( GL_ARRAY_BUFFER, vertices.size() * sizeof( float ), vertices.data(), GL_STATIC_DRAW );
    // - unbind VBO (0 is the default resource ID in OpenGL)
    glBindBuffer( GL_ARRAY_BUFFER, 0 );

    return statusOK;
}

/******************************************************************************
 * Initialize vertex array
 ******************************************************************************/
bool initializeVertexArray()
{
	bool statusOK = true;

	std::cout << "Initialize vertex array..." << std::endl;

	// Create a vertex array to encapsulate all VBO
	// - generate a VAO ID
	glGenVertexArrays( 1, &vertexArray );

	// - bind VAO as current vertex array (in OpenGL state machine)
	glBindVertexArray( vertexArray );
	// - bind VBO as current buffer (in OpenGL state machine)
	glBindBuffer( GL_ARRAY_BUFFER, positionBuffer );

	// - specify the location and data format of the array of generic vertex attributes at index​ to use when rendering
	glVertexAttribPointer( 0/*index of the generic vertex attribute: VBO index (not its ID!)*/, 3/*nb elements in the attribute: (x,y,z)*/, GL_FLOAT/*type of data*/, GL_FALSE/*normalize data*/, 0/*stride*/, 0/*offset in memory*/ );
	// - enable or disable a generic vertex attribute array
	glEnableVertexAttribArray( 0/*index of the generic vertex attribute*/ );

 	// Index buffer
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, indexBuffer );

	// - unbind VAO (0 is the default resource ID in OpenGL)
	glBindVertexArray( 0 );
	// - unbind VBO (0 is the default resource ID in OpenGL)
	glBindBuffer( GL_ARRAY_BUFFER, 0 );


        // Quads

	glGenVertexArrays( 1, &quadVertexArray );
	glBindVertexArray(quadVertexArray );
	glBindBuffer( GL_ARRAY_BUFFER, quadArray );
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
	glEnableVertexAttribArray( 0/*index of the generic vertex attribute*/ );
	glBindVertexArray( 0 );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	return statusOK;
}

/******************************************************************************
 * Initialize shader program
 ******************************************************************************/
bool initializeShaderProgram()
{
	bool statusOK = true;

	std::cout << "Initialize shader program..." << std::endl;

	shaderProgram = glCreateProgram();
	lightShaderProgram = glCreateProgram();

	GLuint vertexShader = glCreateShader( GL_VERTEX_SHADER );
	GLuint fragmentShader = glCreateShader( GL_FRAGMENT_SHADER );
	GLuint lightVertexShader = glCreateShader( GL_VERTEX_SHADER );
	GLuint lightFragmentShader = glCreateShader( GL_FRAGMENT_SHADER );

	const char* vertexShaderSource[] = {
            //	    "#version 330 core                             \n"
	    "#version 130                                  \n"
            " in vec3 aPos; \n"
            " in vec3 aNormal; \n"
	    "                                              \n"
            " out vec3 Normal; \n"
            " out vec3 FragPos; \n"
            " uniform mat4 viewMatrix; \n"
            " uniform mat4 projectionMatrix; \n"
            " uniform mat4 modelMatrix; \n"
	    "                                              \n"
	    "void main( void )                             \n"
	    "{                                             \n"
            " FragPos = vec3(modelMatrix * vec4(aPos, 1.0));\n"
            " Normal = aNormal; \n"
	    " gl_Position =  projectionMatrix * viewMatrix * modelMatrix * vec4(FragPos, 1.0 );      \n"
	    "}                                             \n"
	};
	const char* fragmentShaderSource[] = {
	    "#version 130                                  \n"
	    "                                              \n"
	    " out vec4 FragColor;                              \n"
	    " in vec3 Normal;                              \n"
	    " in vec3 FragPos;                              \n"
            " uniform vec3 lightPos; \n"
            " uniform vec3 lightColor; \n"
            " uniform vec3 objectColor;\n"
	    "                                              \n"
	    "void main( void )                             \n"
	    "{                                             \n"
            "    float ambientStrength = 0.05; \n"
            "    vec3 ambient = ambientStrength * lightColor; \n"
            "    vec3 norm = normalize(Normal); \n"
            "    vec3 lightDir = normalize(lightPos - FragPos); \n"
            "    float diff = max(dot(norm, lightDir), 0.0); \n"
            "    vec3 diffuse = diff * lightColor; \n"
            "    vec3 result = (ambient + diffuse) * objectColor;\n"
	    "    FragColor = vec4(ambient + (objectColor * lightColor * diff), 1.0);       \n"
	    "}                                             \n"
	};
	const char* lightVertexShaderSource[] = {
	    "#version 130                                  \n"
            " in vec3 aPos; \n"
	    "                                              \n"
            " uniform mat4 viewMatrix; \n"
            " uniform mat4 projectionMatrix; \n"
            " uniform mat4 lightModelMatrix; \n"
	    "                                              \n"
	    "void main( void )                             \n"
	    "{                                             \n"
	    " gl_Position =  projectionMatrix * viewMatrix * lightModelMatrix * vec4(aPos, 1.0 );      \n"
	    "}                                             \n"
	};
	const char* lightFragmentShaderSource[] = {
	    "#version 130                                  \n"
	    "                                              \n"
	    " out vec4 FragColor;                              \n"
            " uniform vec3 lightColor; \n"
	    "                                              \n"
	    "void main( void )                             \n"
	    "{                                             \n"
	    "    FragColor = vec4(1.0, 1.0, 1.0, 1.0);       \n"
	    "}                                             \n"
	};

	glShaderSource( vertexShader, 1, vertexShaderSource, nullptr );
	glShaderSource( fragmentShader, 1, fragmentShaderSource, nullptr );
	glShaderSource( lightVertexShader, 1, lightVertexShaderSource, nullptr );
	glShaderSource( lightFragmentShader, 1, lightFragmentShaderSource, nullptr );

	glCompileShader( vertexShader );
	glCompileShader( fragmentShader );
	glCompileShader( lightVertexShader );
	glCompileShader( lightFragmentShader );

	GLint compileStatus;
	glGetShaderiv( vertexShader, GL_COMPILE_STATUS, &compileStatus );
	if ( compileStatus == GL_FALSE )
	{
		std::cout << "Error: vertex shader "<< std::endl;

		GLint logInfoLength = 0;
		glGetShaderiv( vertexShader, GL_INFO_LOG_LENGTH, &logInfoLength );
		if ( logInfoLength > 0 )
		{
			GLchar* infoLog = new GLchar[ logInfoLength ];
			GLsizei length = 0;
			glGetShaderInfoLog( vertexShader, logInfoLength, &length, infoLog );
			std::cout << infoLog << std::endl;
		}
	}

	glGetShaderiv( fragmentShader, GL_COMPILE_STATUS, &compileStatus );
	if ( compileStatus == GL_FALSE )
	{
		std::cout << "Error: fragment shader "<< std::endl;
	}

	glGetShaderiv( lightVertexShader, GL_COMPILE_STATUS, &compileStatus );
	if ( compileStatus == GL_FALSE )
	{
		std::cout << "Error: light vertex shader "<< std::endl;

		GLint logInfoLength = 0;
		glGetShaderiv( lightVertexShader, GL_INFO_LOG_LENGTH, &logInfoLength );
		if ( logInfoLength > 0 )
		{
			GLchar* infoLog = new GLchar[ logInfoLength ];
			GLsizei length = 0;
			glGetShaderInfoLog( lightVertexShader, logInfoLength, &length, infoLog );
			std::cout << infoLog << std::endl;
		}
	}

	glGetShaderiv( lightFragmentShader, GL_COMPILE_STATUS, &compileStatus );
	if ( compileStatus == GL_FALSE )
	{
		std::cout << "Error: light vertex shader "<< std::endl;

		GLint logInfoLength = 0;
		glGetShaderiv( lightFragmentShader, GL_INFO_LOG_LENGTH, &logInfoLength );
		if ( logInfoLength > 0 )
		{
			GLchar* infoLog = new GLchar[ logInfoLength ];
			GLsizei length = 0;
			glGetShaderInfoLog( lightFragmentShader, logInfoLength, &length, infoLog );
			std::cout << infoLog << std::endl;
		}
	}

	glAttachShader( shaderProgram, vertexShader );
	glAttachShader( shaderProgram, fragmentShader );
	glAttachShader( lightShaderProgram, lightVertexShader );
	glAttachShader( lightShaderProgram, lightFragmentShader );

        int success;
        char infoLog[512];

	glLinkProgram( lightShaderProgram );
        // Check link status
        glGetProgramiv(lightShaderProgram, GL_LINK_STATUS, &success);
        if(!success) {
            glGetProgramInfoLog(lightShaderProgram, 512, NULL, infoLog);
            std::cout << "ERROR::LIGHTSHADER::PROGRAM::LINK_FAILED\n" << infoLog << std::endl;
            exit(2);
        }

                
        projectionMatrixLocation = glGetUniformLocation(lightShaderProgram, "projectionMatrix");
        viewMatrixLocation = glGetUniformLocation(lightShaderProgram, "viewMatrix");
        lightModelMatrixLocation = glGetUniformLocation(lightShaderProgram, "lightModelMatrix");
        lightColorLocation = glGetUniformLocation(lightShaderProgram, "lightColor");

	glLinkProgram( shaderProgram );
        // Check link status
        glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
        if(!success) {
            glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
            std::cout << "ERROR::SHADER::PROGRAM::LINK_FAILED\n" << infoLog << std::endl;
            exit(2);
        }

                
        projectionMatrixLocation = glGetUniformLocation(shaderProgram, "projectionMatrix");
        viewMatrixLocation = glGetUniformLocation(shaderProgram, "viewMatrix");
        modelMatrixLocation = glGetUniformLocation(shaderProgram, "modelMatrix");
        lightPosLocation = glGetUniformLocation(shaderProgram, "lightPos");
        lightColorLocation = glGetUniformLocation(shaderProgram, "lightColor");
        objectColorLocation = glGetUniformLocation(shaderProgram, "objectColor");





	return statusOK;
}

/******************************************************************************
 * Callback to display the scene
 ******************************************************************************/
void display( void )
{

	// ...
	glViewport( 0, 0, 640, 480 );

	// Clear the color buffer (of the main framebuffer)
	// - color used to clear
	glClearColor( 0.f, 0.f, 0.f, 0.f );
	glClearDepth( 1.f );
	// - clear the "color" framebuffer
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

        float timeValue = glutGet(GLUT_ELAPSED_TIME);
        modelMatrix = glm::rotate(glm::mat4(), glm::radians(timeValue * 0.01f), glm::vec3(0.2f, 1.f, 0.2f));

        lightPos = glm::rotate(glm::mat4(), glm::radians(timeValue * 0.1f), glm::vec3(1.f, 1.f, 1.f)) * glm::vec4(0.6, 0.6, 0.6, 1.0);

	// Activate shader program
	glUseProgram( lightShaderProgram );

        glUniformMatrix4fv(projectionMatrixLocation, 1, GL_FALSE, glm::value_ptr(projectionMatrix));
        glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, glm::value_ptr(viewMatrix));
        glUniformMatrix4fv(lightModelMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelMatrix));
        glUniform3fv(lightColorLocation, 1, glm::value_ptr(lightColor));

	glBindVertexArray( quadArray );
        glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
	glDrawArrays( GL_QUADS, 0, 1);
        glBindVertexArray( 0 );

	glUseProgram( shaderProgram );

        glUniformMatrix4fv(projectionMatrixLocation, 1, GL_FALSE, glm::value_ptr(projectionMatrix));
        glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, glm::value_ptr(viewMatrix));
        glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelMatrix));
        glUniform3fv(lightPosLocation, 1, glm::value_ptr(lightPos));
        glUniform3fv(lightColorLocation, 1, glm::value_ptr(lightColor));
        glUniform3fv(objectColorLocation, 1, glm::value_ptr(objectColor));

	// - bind VAO as current vertex array (in OpenGL state machine)
	glBindVertexArray( vertexArray );

	// Render
	//glDrawArrays( GL_TRIANGLES, 0, 3 );
        //glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
        glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
        // Draw the triangles !
        glDrawElements(
         GL_TRIANGLES,      // mode
         numberOfIndices_,    // count
         GL_UNSIGNED_INT,   // type
         (void*)0           // element array buffer offset
         );
        // - unbind VAO (0 is the default resource ID in OpenGL)
        glBindVertexArray( 0 );


	// Deactivate current shader program
	glUseProgram( 0 );

	// OpenGL commands are not synchrone, but asynchrone (stored in a "command buffer")
	glFlush();
	// Swap buffers for "double buffering" display mode (=> swap "back" and "front" framebuffers)
	glutSwapBuffers();
}

// What to do when there is nothing to do
void display_idle(void)
{
        glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y)
{
	printf("key = %c = %d\n", key, key);
	if(key==113) exit(0); // Touche Escape : quitter le programme

        // TOP: I : 105
        // LEFT : J : 106
        // RIGHT : L : 108
        // BOTTOM : K :107
        // Reset : R : 114

        if(key==105) eyez += 0.1f;
        else if(key==106) eyex -= 0.1f;
        else if(key==108) eyex += 0.1f;
        else if(key==107) eyez -= 0.1f;
        else if(key==114)
        {
            eyez = 1.f;
            eyex = 0.f;

        }

        updateViewMatrix();

	glutPostRedisplay(); 
}

/******************************************************************************
 * Main function
 ******************************************************************************/
int main( int argc, char** argv )
{
	std::cout << "Exo 1 - OpenGL with GLUT" << std::endl;

	// Initialize the GLUT library
	glutInit( &argc, argv );

//glutInitContextVersion (3,3);
//glutInitContextProfile ( GLUT_COMPATIBILITY_PROFILE );


	// - configure the main framebuffer to store rgba colors,
	//   and activate double buffering (for fluid/smooth visualization)
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE );
	// - window size and position
	glutInitWindowSize( 640, 480 );
	glutInitWindowPosition( 50, 50 );
	// - create the window
	glutCreateWindow( "Exo 1" );
	glutKeyboardFunc(keyboard);
	// - callback when displaying window (user custom fonction pointer: "void f( void )")
	glutDisplayFunc( display );
        glutIdleFunc(display_idle);

	// Initialize the GLEW library
	// - mandatory to be able to use OpenGL extensions,
	//   because OpenGL core API is made of OpenGL 1 and other functions are null pointers (=> segmentation fault !)
	//   Currently, official OpenGL version is 4.5 (or 4.6)
	GLenum error = glewInit();
	if ( error != GLEW_OK )
	{
		fprintf( stderr, "Error: %s\n", glewGetErrorString( error ) );
		exit( -1 );
	}

	// Initialize all your resources (graphics, data, etc...)
	initialize();

	// Enter the GLUT main event loop (waiting for events: keyboard, mouse, refresh screen, etc...)
	glutMainLoop();
}

