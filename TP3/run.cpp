
/*
▄▄▄▄▄▄▄ ▄▄▄▄▄   ▄▄▄▄ 
   █    █   ▀█ ▀   ▀█
   █    █▄▄▄█▀   ▄▄▄▀
   █    █          ▀█
   █    █      ▀▄▄▄█▀
*/
                     
/******************************************************************************
 ******************************* INCLUDE SECTION ******************************
 ******************************************************************************/

// STL
#include <iostream>
#include <vector>

// System
#include <cstdio>
#include <cmath>
#include <unistd.h>
// Graphics
// - GLEW (always before "gl.h")
#include <GL/glew.h>
// - GL
#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/gl.h>
// - GLUT
#include <GL/glut.h>

// - GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
/******************************************************************************
 ****************************** NAMESPACE SECTION *****************************
 ******************************************************************************/

/******************************************************************************
 ************************* DEFINE AND CONSTANT SECTION ************************
 ******************************************************************************/

// VBO (vertex buffer object) : used to store positions coordinates at each point
GLuint positionBuffer;
// VAO (vertex array object) : used to encapsulate several VBO
GLuint vertexArray;
// Shader program, contiendra le vertex shader et le fragment shader
GLuint shaderProgram;
// Pointer to uniform shader var
GLint transformLocation;

// View Matrix
// User parameters
glm::vec3 eye = glm::vec3(0.f, 0.f, 5.f);
glm::vec3 center = glm::vec3(0.f, 0.f, 0.f);
glm::vec3 up = glm::vec3(0.f, 1.f, 0.f);
// View matrix
glm::mat4 viewMatrix = glm::lookAt(eye, center, up);

// Projection matrix
float fovY = 45.f;
float aspect = 1.f;
float zNear = 0.f;
float zFar = 200.f;
glm::mat4 projectionMatrix = glm::perspective(fovY, aspect, zNear, zFar);

// Model matrix
glm::mat4 modelMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(0.5f));

// Transform
glm::mat4 transformMatrix;

/******************************************************************************
 ***************************** TYPE DEFINITION ********************************
 ******************************************************************************/

/******************************************************************************
***************************** METHOD DEFINITION ******************************
******************************************************************************/

/******************************************************************************
 ***************************** METHOD DEFINITION ******************************
 ******************************************************************************/

bool initialize();
bool checkExtensions();
bool initializeArrayBuffer();
bool initializeVertexArray();
bool initializeShaderProgram();
bool finalize();

/******************************************************************************
 * Initialize all
 ******************************************************************************/
bool initialize()
{
	std::cout << "Initialize all..." << std::endl;

	bool statusOK = true;

	if ( statusOK )
	{
		checkExtensions();
	}

	if ( statusOK )
	{
		initializeArrayBuffer();
	}

	if ( statusOK )
	{
		initializeVertexArray();
	}

	if ( statusOK )
	{
		initializeShaderProgram();
	}

	return statusOK;
}

/******************************************************************************
 * Finalize all
 ******************************************************************************/
bool finalize()
{
	bool statusOK = true;

	std::cout << "Finalize all..." << std::endl;

	return statusOK;
}

/******************************************************************************
 * Check GL extensions
 ******************************************************************************/
bool checkExtensions()
{
	bool statusOK = true;

	std::cout << "Check GL extensions..." << std::endl;

	return statusOK;
}

/******************************************************************************
 * Initialize array buffer
 ******************************************************************************/
bool initializeArrayBuffer()
{
	bool statusOK = true;

	std::cout << "Initialize array buffer..." << std::endl;

	// In this example, we want to display one triangle

	// Buffer of positions on CPU (host)
	// - BEWARE: counter-clock wise !
	// IMPORTANT: here, data have to be placed in NDC space (normalized device coordinates system),
	//            i.e. in a cube from [-1;-1] (in front of screen) to [1;1] (behind screen)
	// Data is stored as (x,y,z) triplets: (x1,y1,z1,x2,y2,z2,x3,y3,z3)
	const std::vector< float > vertices = {
        // FRONT
	-0.5f, -0.5f, .5f, 0.f, 0.f, 1.f,
	 0.5f, -0.5f, .5f, 0.f, 0.f, 1.f,
	 0.5f,  0.5f, .5f, 0.f, 0.f, 1.f,
	 -0.5f, 0.5f, .5f, 0.f, 0.f, 1.f,
        
         // BACK

	 -0.5f, 0.5f, -5.f, 1.f, 0.f, 0.f,
	 0.5f,  0.5f, -5.f, 1.f, 0.f, 0.f,
	 0.5f, -0.5f, -5.f, 1.f, 0.f, 0.f,
	-0.5f, -0.5f, -5.f, 1.f, 0.f, 0.f
	};

	// Create a GPU buffer (device)
	// - generate a VBO ID
	glGenBuffers( 1, &positionBuffer );
	// - bind VBO as current buffer (in OpenGL state machine)
	glBindBuffer( GL_ARRAY_BUFFER, positionBuffer );
	// - send data from CPU (host) to GPU (device)
	glBufferData( GL_ARRAY_BUFFER, vertices.size() * sizeof( float ), vertices.data(), GL_STATIC_DRAW );
	// - unbind VBO (0 is the default resource ID in OpenGL)
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	return statusOK;
}

/******************************************************************************
 * Initialize vertex array
 ******************************************************************************/
bool initializeVertexArray()
{
	bool statusOK = true;

	std::cout << "Initialize vertex array..." << std::endl;

	// Create a vertex array to encapsulate all VBO
	// - generate a VAO ID
	glGenVertexArrays( 1, &vertexArray );

	// - bind VAO as current vertex array (in OpenGL state machine)
	glBindVertexArray( vertexArray );
	// - bind VBO as current buffer (in OpenGL state machine)
	glBindBuffer( GL_ARRAY_BUFFER, positionBuffer );

	// - specify the location and data format of the array of generic vertex attributes at index​ to use when rendering
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), 0);
	// - enable or disable a generic vertex attribute array
	glEnableVertexAttribArray( 0/*index of the generic vertex attribute*/ );
        // Colors
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	// - unbind VAO (0 is the default resource ID in OpenGL)
	glBindVertexArray( 0 );
	// - unbind VBO (0 is the default resource ID in OpenGL)
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	return statusOK;
}

/******************************************************************************
 * Initialize shader program
 ******************************************************************************/
bool initializeShaderProgram()
{
	bool statusOK = true;

	std::cout << "Initialize shader program..." << std::endl;

        shaderProgram = glCreateProgram();

        GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
        GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

        const char* vertexShaderSource[] = {
            "#version 130                                  \n"
            "                                              \n"
            " out vec3 outColor;                        \n"
            " in vec3 position;                         \n"
            " in vec3 color;                                \n"
            " uniform mat4 transformMatrix;                       \n"
            "                                              \n"
            "void main( void )                             \n"
            "{                                             \n"
            "    gl_Position = transformMatrix * vec4(position, 1.0);\n"
            "    outColor = vec3(color.x, color.y, color.z);\n"
            "}                                             \n"
        };
        const char* fragmentShaderSource[] = {
            "#version 130                                  \n"
            "                                              \n"
            " in vec3 outColor;                        \n"
            " out vec4 color;                              \n"
            "                                              \n"
            "void main( void )                             \n"
            "{                                             \n"
            "    color = vec4(outColor, 1.0);                       \n"
            "}                                             \n"
        };

        glShaderSource( vertexShader, 1, vertexShaderSource, nullptr );
        glShaderSource( fragmentShader, 1, fragmentShaderSource, nullptr );
        

        // Compilation du vertex shader
        glCompileShader(vertexShader);
        // Check compilation status
        int  success;
        char infoLog[512];

        glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
        if(!success)
        {
            glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
            std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
            exit(2);
        }

        // Compilation du fragment shader
        glCompileShader(fragmentShader);
        // Check compilation status
        glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
        if(!success)
        {
            glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
            std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
            exit(2);
        }
        
        // Attch les shader
        glAttachShader(shaderProgram, vertexShader);
        glAttachShader(shaderProgram, fragmentShader);
        glLinkProgram(shaderProgram);

        // Check link status
        glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
        if(!success) {
            glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
            std::cout << "ERROR::SHADER::PROGRAM::LINK_FAILED\n" << infoLog << std::endl;
            exit(2);
        }

        transformLocation = glGetUniformLocation(shaderProgram, "transformMatrix");

	return statusOK;
}

/******************************************************************************
 * Callback to display the scene
 ******************************************************************************/
void display( void )
{

	// ...
	//glViewport( 0, 0, 640, 480 );

	// Clear the color buffer (of the main framebuffer)
	// - color used to clear
	glClearColor( 0.f, 0.f, 0.f, 0.f );
	glClearDepth( 1.f );
	// - clear the "color" framebuffer
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    
        float timeValue = glutGet(GLUT_ELAPSED_TIME);
        float greenValue = (sin(timeValue*0.001f) / 2.0f) + 0.5f;

        //eye.z = 1.0f - timeValue * 0.0001f;
        //viewMatrix = glm::lookAt(eye, center, up);
        modelMatrix = glm::rotate(glm::mat4(), glm::radians(timeValue * 0.01f), glm::vec3(0.2f, 1.f, 0.2f));
        transformMatrix = projectionMatrix * viewMatrix * modelMatrix;

        // Utilisation du shader

        glUseProgram(shaderProgram);
        glUniformMatrix4fv(transformLocation, 1, GL_FALSE, glm::value_ptr(transformMatrix));

        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	// Render user custom data
	// - for this example, store a default common color for all points (in OpenGL state machine)
	//glColor3f( 1.f, 0.f, 0.f );
	// - bind VAO as current vertex array (in OpenGL state machine)
	glBindVertexArray( vertexArray );
	// - render primitives from array data (here interpreted as primitives of type "triangles")
	//   => pass the first index of points and their numbers (1 triangle made of 3 points)
	glDrawArrays( GL_QUADS, 0, 4 );
	glDrawArrays( GL_QUADS, 1, 4 );
	// - unbind VAO (0 is the default resource ID in OpenGL)
	glBindVertexArray( 0 );
        // Fini avec le shader
        glUseProgram(0);

	// OpenGL commands are not synchrone, but asynchrone (stored in a "command buffer")
	glFlush();
	// Swap buffers for "double buffering" display mode (=> swap "back" and "front" framebuffers)
	glutSwapBuffers();

}

// What to do when there is nothing to do
void display_idle(void)
{
        glutPostRedisplay();
}

/******************************************************************************
 * Main function
 ******************************************************************************/
int main( int argc, char** argv )
{
	std::cout << "Exo 1 - OpenGL with GLUT" << std::endl;
        glm::vec4 vec(1.0f, 0.0f, 0.0f, 1.0f);
        glm::mat4 trans;
        trans = glm::translate(trans, glm::vec3(1.0f, 1.0f, 0.0f));
        vec = trans * vec;
        std::cout << vec.x << vec.y << vec.z << std::endl;

	// Initialize the GLUT library
	glutInit( &argc, argv );

	// - configure the main framebuffer to store rgba colors,
	//   and activate double buffering (for fluid/smooth visualization)
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE );
	// - window size and position
	glutInitWindowSize( 640, 480 );
	glutInitWindowPosition( 50, 50 );
	// - create the window
	glutCreateWindow( "Exo 1" );
	// - callback when displaying window (user custom fonction pointer: "void f( void )")
	glutDisplayFunc( display );
        glutIdleFunc(display_idle);

	// Initialize the GLEW library
	// - mandatory to be able to use OpenGL extensions,
	//   because OpenGL core API is made of OpenGL 1 and other functions are null pointers (=> segmentation fault !)
	//   Currently, official OpenGL version is 4.5 (or 4.6)
	GLenum error = glewInit();
	if ( error != GLEW_OK )
	{
		fprintf( stderr, "Error: %s\n", glewGetErrorString( error ) );
		exit( -1 );
	}

	// Initialize all your resources (graphics, data, etc...)
	initialize();

	// Enter the GLUT main event loop (waiting for events: keyboard, mouse, refresh screen, etc...)
	glutMainLoop();
}

