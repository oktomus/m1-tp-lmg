/******************************************************************************
 ******************************* INCLUDE SECTION ******************************
 ******************************************************************************/

// STL
#include <iostream>
#include <vector>

// Graphics
#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glut.h>

/******************************************************************************
 ****************************** NAMESPACE SECTION *****************************
 ******************************************************************************/

/******************************************************************************
 ************************* DEFINE AND CONSTANT SECTION ************************
 ******************************************************************************/

int windowName;
GLuint vbo; // Vertex array
GLuint shaderProgram;
GLuint fragmentShader;

/******************************************************************************
 ***************************** TYPE DEFINITION ********************************
 ******************************************************************************/

/******************************************************************************
 ***************************** METHOD DEFINITION ******************************
 ******************************************************************************/

/******************************************************************************
 ***************************** METHOD DEFINITION ******************************
 ******************************************************************************/

bool initialize();
bool checkExtensions();
bool initializeArrayBuffer();
bool initializeVertexArray();
bool initializeShaderProgram();
bool finalize();

/******************************************************************************
 * Initialize all
 ******************************************************************************/

bool initialize()
{
    std::cout << "Initialize all..." << std::endl;

    bool statusOK = true;

    if ( statusOK )
    {
        statusOK = checkExtensions();
    }

    if ( statusOK )
    {
        statusOK = initializeArrayBuffer();
    }

    if ( statusOK )
    {
        statusOK = initializeVertexArray();
    }

    if ( statusOK )
    {
        statusOK = initializeShaderProgram();
    }

    return statusOK;
}

/******************************************************************************
 * Finalize all
 ******************************************************************************/
bool finalize()
{
    bool statusOK = true;

    std::cout << "Finalize all..." << std::endl;

    return statusOK;
}

/******************************************************************************
 * Check GL extensions
 ******************************************************************************/
bool checkExtensions()
{
    bool statusOK = true;

    std::cout << "Check GL extensions..." << std::endl;

    std::cout << "GLEW..." << std::endl;

    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        std::cerr << "Error: " << glewGetErrorString(err) << std::endl;
        exit(2);
    }
    std::cout << "Status: Using GLEW " << glewGetString(GLEW_VERSION)
        << std::endl;

    return statusOK;
}

/******************************************************************************
 * Initialize array buffer
 ******************************************************************************/
bool initializeArrayBuffer()
{
    bool statusOK = true;

    std::cout << "Initialize array buffer..." << std::endl;



    glGenBuffers(1, &vbo); // Genere un buffer
    glBindBuffer(GL_ARRAY_BUFFER, vbo); // Flag le buffer

    float vertices[] = {
        -0.5f,  -0.5f,
        0.5f, -0.5f, 
        0.5f, 0.5f  
    };

    // Remplis le bugger
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    return statusOK;
}

/******************************************************************************
 * Initialize vertex array
 ******************************************************************************/
bool initializeVertexArray()
{
    bool statusOK = true;

    std::cout << "Initialize vertex array..." << std::endl;

    return statusOK;
}

/******************************************************************************
 * Initialize shader program
 ******************************************************************************/
bool initializeShaderProgram()
{
    bool statusOK = true;
    /*

    std::cout << "Initialize shader program..." << std::endl;

    const char* shaderSource = R"glsl(
            #version 330 core
            layout (location = 0) in vec2 aPos;

            void main()
            {
                gl_Position = vec4(aPos.x, aPos.y, 1.0, 1.0);
            }
	)glsl";

    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexSource, NULL);
    glCompileShader(vertexShader);

    const char* fragmentSource = R"glsl(
	    #version 150 core

	    out vec4 outColor;

	    void main()
	    {
		outColor = vec4(1.0, 1.0, 1.0, 1.0);
	    }
	)glsl";

    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
    glCompileShader(fragmentShader);

    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glBindFragDataLocation(shaderProgram, 0, "outColor");
    glLinkProgram(shaderProgram);

    glUseProgram(shaderProgram);
    */

    return statusOK;
}




/******************************************************************************
 * Callback to display the scene
 ******************************************************************************/
void display( void )
{
    std::cout << "Callback: display..." << std::endl;

    glClearColor(0.1f, 0.2f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Basic way
    /*glBegin(GL_TRIANGLES);
      glColor3f(1.0f, 0.0f, 0.0f); 
      glVertex2f(-0.5f, -0.5f);    // x, y
      glColor3f(0.0f, 0.0f, 1.0f); 
      glVertex2f( 0.5f, -0.5f);
      glColor3f(0.0f, 1.0f, 0.0f); 
      glVertex2f( 0.5f,  0.5f);
      glVertex2f(-0.5f, -0.5f);
      glColor3f(0.0f, 0.0f, 1.0f);   
      glVertex2f( -0.5f, 0.5f);
      glColor3f(1.0f, 0.0f, 0.0f); 
      glVertex2f( 0.5f,  0.5f);

      glEnd();*/



    // 0: vertices of the VOB
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    glVertexAttribPointer(
            0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
            2,                  // size
            GL_FLOAT,           // type
            GL_FALSE,           // normalized?
            0,                  // stride
            (void*)0            // array buffer offset
            );

    // Draw the triangle !
    glDrawArrays(GL_TRIANGLES, 0, 3); // 3 * 2
    glDisableVertexAttribArray(0);

    glFlush();  // Render now
    glutSwapBuffers();
}


/******************************************************************************
 * Main function
 ******************************************************************************/
int main( int argc, char** argv )
{
    std::cout << "Exo 1 - OpenGL with GLUT" << std::endl;

    glutInit(&argc, argv);	
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowSize(480,480); 	//Optionnel
    windowName = glutCreateWindow("OpenGL !");

    // Initialize all your resources (graphics, etc...)
    initialize();

    glutDisplayFunc(display);
    glutMainLoop();


    return 0;

}
