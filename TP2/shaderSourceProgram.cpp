const char* vertexShaderSource[] = {
    //	    "#version 330 core                             \n"
    "#version 130                                  \n"
        "                                              \n"
        " in vec3 position;                            \n"
        "                                              \n"
        "void main( void )                             \n"
        "{                                             \n"
        "    gl_Position = vec4( position, 1.0 );      \n"
        "}                                             \n"
};
const char* fragmentShaderSource[] = {
    "#version 130                                  \n"
        "                                              \n"
        " out vec4 color;                              \n"
        "                                              \n"
        "void main( void )                             \n"
        "{                                             \n"
        "    color = vec4( 1.0, 0.0, 0.0, 1.0 );       \n"
        "}                                             \n"
};

glShaderSource( vertexShader, 1, vertexShaderSource, nullptr );
glShaderSource( fragmentShader, 1, fragmentShaderSource, nullptr );
